<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="../common/meta.jsp"/>
</head>

<body>
<jsp:include page="../common/header.jsp"/>

<div class="container">
    <div>
        <ol class="breadcrumb">
            <li><a href="/admin/bookList"><i class="fa fa-long-arrow-left" aria-hidden="true">Go back</i></a></li>
            <li><a href="/admin/updateBook?id=${book.id}">Edit</a></li>
        </ol>

    </div>

    <div class="row">
        <div class="col-xs-3">
            <div class="panel panel-default">
                <div class="panel-body">
                    <img src="../static/image/${book.id}.png" class="img-responsive" />
                </div>
            </div>
        </div>
        <div class="col-xs-9">
            <h3>${book.title}</h3>
            <div class="row">
                <div class="col-xs-6">
                    <p><strong>Author: </strong><span>${book.author}</span></p>
                    <p><strong>Publisher: </strong><span>${book.publisher}</span></p>
                    <p><strong>Publication Date: </strong><span>${book.publicationDate}</span></p>
                    <p><strong>Language: </strong><span>${book.language}/span></p>
                    <p><strong>Category: </strong><span >${book.category}</span></p>
                    <p><strong>Pages: </strong><span >${book.numberOfPages}</span></p>
                </div>
                <div class="col-xs-6">
                    <p><strong>Format: </strong><span>${book.format}</span></p>
                    <p><strong>ISBN: </strong><span>${book.isbn}</span></p>
                    <p><strong>Shipping Weight: </strong><span>${book.shippingWeight}</span></p>
                    <p><strong>List Price: </strong><span>${book.listPrice}</span></p>
                    <p><strong>Our Price: </strong><span>${book.ourPrice}</span></p>
                    <p><strong>Number in Stock: </strong><span>${book.stockNumber}</span></p>
                </div>
            </div>

            <p><Strong>Description: </Strong><span>${book.description}</span></p>
        </div>

    </div>
</div>

</body>
</html>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!doctype html>
<html lang="en">
<head>
    <jsp:include page="../common/meta.jsp"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <h1>Book List</h1>
</head>

<body>
<jsp:include page="../common/headerAdmin.jsp"/>

<div class="container">
    <div class="table-responsive">
        <table id="bookListTable"
               class="table table-hover table-vcenter text-nowrap table_custom border-style list">
            <thead>
            <tr>
                <th>Title</th>
                <th>Author</th>
                <th>Category</th>
                <th>List Price</th>
                <th>Our Price</th>
                <th>Active</th>
                <th>Edit/View/Delete</th>
            </tr>
            </thead>
            <tbody>


            </tbody>
        </table>

    </div>
</div>



<script src="../static/js/core.js"></script>
<script src="../static/js/index.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function () {
        $('#bookListTable').DataTable({
            "lengthMenu": [[4, 10, 25, 50, -1], [4, 10, 25, 50, "All"]],
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "getBookList",
                async: false
            }
        });

    });

</script>


</body>
</html>

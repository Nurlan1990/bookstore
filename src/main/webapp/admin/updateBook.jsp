<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 11/17/2020
  Time: 7:38 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
<jsp:include page="../common/meta.jsp"/>
</head>

<body>
<jsp:include page="../common/header.jsp"/>

<div class="container">
    <div class="row">
        <form class="form-horizontal" action="/admin/updateBook" method="post" enctype="multipart/form-data">
            <fieldset>
                <legend class="center-block">Update Book</legend>
                <span style="font-size: small"> * is a required field</span>

                <input hidden="hidden" name="id" value="${book.id}" />

                <!-- title -->
                <div class="form-group">
                    <label for="title">* Title</label> <input type="text" name="title"
                                                              class="form-control" id="title" value="${book.title}"
                                                              required="required" placeholder="Title" /> <span
                        class="help-block">Title of the book</span>
                </div>

                <!-- author -->
                <div class="form-group">
                    <label for="author">* Author</label> <input type="text" name="author"
                                                                class="form-control" id="author" value="${book.author}"
                                                                required="required" placeholder="Author" /> <span
                        class="help-block">Author of the book</span>
                </div>

                <!-- publisher -->
                <div class="form-group">
                    <label for="publisher">Publisher</label> <input type="text" name="publisher"
                                                                    class="form-control" id="publisher" value="${book.publisher}"
                                                                    placeholder="Publisher" /> <span class="help-block">Publisher
							of the book</span>
                </div>

                <!-- publication date -->
                <div class="form-group">
                    <label for="publicationDate">Publication Date</label> <input
                        type="date" class="form-control" id="publicationDate" name="publicationDate"
                        value="${book.publicationDate}" placeholder="ublication Date" />
                    <span class="help-block">Publication Date of the book</span>
                </div>

                <!-- language -->
                <div class="form-group">
                    <label class="col-xs-2 control-label" for="language">Language</label>
                    <div class="col-xs-8">
                        <select value="${book.language}" id="language" name="language"
                                class="form-control">
                            <option value="english">English</option>
                            <option value="spanish">Spanish</option>
                        </select>
                    </div>
                </div>

                <!-- category -->
                <div class="form-group">
                    <label class="col-xs-2 control-label" for="category">*
                        Category</label>
                    <div class="col-xs-8">
                        <select value="${book.category}" id="category" name="category"
                                class="form-control">
                            <option value="" selected="selected" disabled="disabled">Please
                                select an option...</option>
                            <option value="Management"
                                    selected="(${book.category}=='Management')">Management</option>
                            <option value="Fiction"
                                    selected="(${book.category}=='Fiction')">Fiction</option>
                            <option value="Engineering"
                                    selected="(${book.category}=='Engineering')">Engineering</option>
                            <option value="Programming"
                                    selected="(${book.category}=='Programming')">Programming</option>
                            <option value="Arts &amp; Literature"
                                    selected="(${book.category}=='Arts &amp; Literature')">Arts
                                &amp; Literature</option>
                        </select>
                    </div>
                </div>

                <!-- pages -->
                <div class="form-group">
                    <label for="pageNumber">Pages</label> <input type="number" name="numberOfPages"
                                                                 class="form-control" id="pageNumber"
                                                                 value="${book.numberOfPages}" placeholder="Page Number" /> <span
                        class="help-block">Number of pages of the book</span>
                </div>

                <!-- format -->
                <div class="form-group">
                    <label class="col-xs-2 control-label" for="format">Format</label>
                    <div class="col-xs-8">
                        <select value="${book.format}" id="format" name="format"
                                class="form-control">
                            <option value="paperback">Paperback</option>
                            <option value="hardcover">Hardcover</option>
                        </select>
                    </div>
                </div>

                <!-- isbn -->
                <div class="form-group">
                    <label for="isbn">ISBN</label> <input type="number" name="isbn"
                                                          class="form-control" id="isbn" value="${book.isbn}"
                                                          placeholder="ISBN" /> <span class="help-block">ISBN of
							the book</span>
                </div>

                <!-- shipping weight -->
                <div class="form-group">
                    <label class="col-xs-2 control-label" for="shippingWeight">Shipping
                        Weight</label>
                    <div class="col-xs-8">
                        <div class="input-group">
                            <input value="${book.shippingWeight}" type="number" name="shippingWeight"
                                   class="form-control" id="shippingWeight"
                                   placeholder="Shipping Weight" />
                            <span class="input-group-addon">Ounces</span>
                        </div>
                        <span class="help-block">Shipping Weight of the book</span>
                    </div>
                </div>

                <!-- list price -->
                <div class="form-group">
                    <label class="col-xs-2 control-label" for="listPrice">List Price</label>
                    <div class="col-xs-8">
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input value="${book.listPrice}" type="number" name="listPrice" step="0.01"
                                   class="form-control" id="listPrice"
                                   placeholder="List Price" />
                        </div>
                        <span class="help-block">List price of the book</span>
                    </div>
                </div>

                <!-- our price -->
                <div class="form-group">
                    <label class="col-xs-2 control-label" for="ourPrice">Our Price</label>
                    <div class="col-xs-8">
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input value="${book.ourPrice}" type="number" name="ourPrice" step="0.01"
                                   class="form-control" id="ourPrice"
                                   placeholder="Our Price" />
                        </div>
                        <span class="help-block">Our price of the book</span>
                    </div>
                </div>

                <!-- in stock number -->
                <div class="form-group">
                    <label class="col-xs-2 control-label" for="stockNumber">Number in stock</label>
                    <div class="col-xs-8">
                        <input value="${book.stockNumber}" type="number" name="stockNumber"
                               class="form-control" id="stockNumber"
                               placeholder="Number of in-stock copies" />
                        <span class="help-block">Number of in-stock copies</span>
                    </div>
                </div>

                <!-- status of book -->
                <div class="form-group">
                    <label class="col-xs-2 control-label" for="status">Status</label>
                    <div class="col-xs-8">
                        <label><input value="true" type="radio" name="active" checked="checked"
                                      id="status"
                        /> Active </label>
                        <label><input value="false" type="radio" name="active"

                        /> Inactive </label>
                    </div>
                </div>

                <!-- description -->
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea class="form-control" name="description" id="description" rows="3" >${book.description}</textarea>
                    <span class="help-block">Description of the book</span>
                </div>

                <!-- upload image -->
                <div class="form-group">
                    <div class="col-xs-2">
                        <label for="bookImage">Upload book image</label>
                    </div>
                    <div class="col-xs-8">
                        <input id="bookImage" type="file" name="bookImage" value="${book.bookImage}" />
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success">Update Book</button>
                    <a class="btn btn-danger" href="/admin/">Cancel</a>
                </div>
            </fieldset>
        </form>
    </div>
</div>

</body>
</html>


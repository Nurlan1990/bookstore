<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 11/15/2020
  Time: 10:26 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="../common/meta.jsp"/>
</head>

<body>
<jsp:include page="../common/header.jsp"/>

<div class="container">
    <div class="row" style="margin-bottom: -100px;">
        <div class="col-xs-8">
            <h2 class="section-headline">
                <h2 class="section-headline">
                    <span>Book detail</span>
                </h2>
            </h2>
        </div>
        <div class="col-xs-4">
            <img src="../static/image/logo.jpg" class="img-responsive"/>
        </div>
    </div>
    <hr
            style="position: absolute; width: 100%; height: 6px; background-color: #333; z-index: -1; margin-top: -80px;"/>
    <img class="img-responsive" src="../static/image/wood.jpeg"
         style="margin-top: -75px;"/>


    <form action="shoppingCart/addItem" method="post">
        <input type="hidden" name="id" value="${book.id}"/>
        <div class="row" style="margin-top: 120px;">
            <div class="col-xl-3">
                <a href="/bookSheif">Back to book list</a><br/>
                <img class="img-responsive shelf-book" src="../static/image/${book.id}.png" width="50" height="50"/>
            </div>

            <div class="col-xl-9">
                <h3>${book.title}</h3>
                <c:if test="${notEnoughStock==true}">
                    <span>Not Enough Stock</span>
                </c:if>
                <div class="row">
                    <div class="col-xl-5">
                        <h5><strong>Author: </strong><span>${book.author}</span></h5>
                        <p><strong>Publisher: </strong><span>${book.publisher}</span></p>
                        <p><strong>Publication Date: </strong><span>${book.publicationDate}</span></p>
                        <p><strong>Language: </strong><span>${book.language}</span></p>
                        <p><strong>Category: </strong><span>${book.category}</span></p>
                        <p><strong><span>${book.format}</span> </strong><span>${book.numberOfPages}</span> pages</p>
                        <p><strong>ISBN: </strong><span>${book.isbn}</span></p>
                        <p><strong>Shipping Weight: </strong><span>${book.shippingWeight}</span> ounces</p>
                    </div>
                    <div class="col-xs-7">
                        <div class="panel panel-default" style="border-width: 5px; margin-top: 20px;">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <h4>Our Price: <span
                                                style="color:#db3208;">$<span>${book.ourPrice}</span></span></h4>
                                        <p>List Price: <span
                                                style="text-decoration: line-through">$<span>${book.listPrice}</span></span>
                                        </p>
                                        <p>You save: $<span>${book.listPrice - book.ourPrice}</span>
                                        </p>
                                        <span>Qty: </span>
                                        <select name="qty">
                                            <c:forEach items="${qytList}" var="qty">
                                                <option value="${qty}">${qty}</option>
                                            </c:forEach>
                                        </select>

                                    </div>
                                    <div class="col-xs-6">
                                        <c:if test="${book.stockNumber>=10}">
                                            <h4 style="color: green">${book.stockNumber} In Stock</h4>
                                        </c:if>
                                        <c:if test="${book.stockNumber<10 && book.stockNumber>0}">
                                            <h4 style="color: green">Only <span>${book.stockNumber} In Stock</span></h4>
                                        </c:if>
                                        <c:if test="${book.stockNumber==0}">
                                            <h4 style="color:darkred;">Unavailable </h4>&nbsp;
                                        </c:if>

                                        <button type="submit" class="btn btn-warning"
                                                style="color:black;border:1px solid black; padding: 10px 40px 10px 40px;">
                                            Add to Cart
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr/>
                <p>${book.description}</p>
            </div>
        </div>
    </form>

    <!-- end of container -->


</div>
</body>
</html>


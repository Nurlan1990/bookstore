<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!doctype html>
<html lang="en">
<head>
    <jsp:include page="../common/meta.jsp"/>
</head>

<body>
<jsp:include page="../common/header.jsp"/>

<div class="container">
    <div class="row">
        <div class="col-xl-8">
            <h2 class="section-headline"><span>${user.username} profile</span></h2>
        </div>
        <div class="col-xl-4">
            <img src="../static/image/rap.jpg" class="img-responsive" width="200" height="200"/>
        </div>
        <hr/>
        <img src="../static/image/wood.jpeg" class="img-responsive"/>

        <div class="row">
            <div class="col-xl-9">
                <ul class="nav nav-tabs">
                    <li><a href="#tab-1" data-toggle="tab" class="nav-link"><span style="color:red;">Edit</span></a>
                    </li>&nbsp;
                    <li><a href="#tab-2" data-toggle="tab" class="nav-link"><span style="color:red;">Orders</span></a>
                    </li>&nbsp;
                    <c:choose>
                        <c:when test="${classActiveBilling==true}">
                            <li><a href="#tab-3" data-toggle="tab" class="nav-link active"><span style="color:red;">Billing</span></a>
                            </li>

                            &nbsp;
                        </c:when>
                        <c:otherwise>
                            <li><a href="#tab-3" data-toggle="tab" class="nav-link"><span
                                    style="color:red;">Billing</span></a></li>
                            &nbsp;
                        </c:otherwise>
                    </c:choose>
                    <c:choose>
                        <c:when test="${classActiveShipping==true}">
                            <li><a href="#tab-4" data-toggle="tab" class="nav-link active"><span style="color:red;">Shiping</span></a>
                            </li>
                            &nbsp;
                            &nbsp;
                        </c:when>
                        <c:otherwise>
                            <li><a href="#tab-4" data-toggle="tab" class="nav-link"><span
                                    style="color:red;">Shiping</span></a>
                            </li>
                            &nbsp;
                            &nbsp;
                        </c:otherwise>
                    </c:choose>


                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade" id="tab-1">
                        <div class="card-group">
                            <div class="card" style="border: none;">
                                <div class="card-body">
                                    <form action="updateUserInfo" method="post">
                                        <input type="hidden" name="id" value="${user.id}"/>

                                        <div class="bg-info">
                                            <c:if test="${UserInfoUpdated == true}">
                                                User info updated.
                                            </c:if>
                                        </div>

                                        <div class="form-group">
                                            <label for="firstName">First name</label>
                                            <input type="text" class="form-control" id="firstName" name="firstName"
                                                   value="${user.firstName}"/>
                                        </div>
                                        <div class="form-group">

                                            <label for="lastName">Last name</label>
                                            <input type="text" class="form-control" id="lastName" name="lastName"
                                                   value="${user.lastName}"/>

                                        </div>
                                        <div class="form-group">
                                            <label for="userName">Username</label>
                                            <input type="text" class="form-control" id="userName" name="username"
                                                   value="${user.username}"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="currentPassword">Current Password</label>
                                            <input type="password" class="form-control" id="currentPassword"
                                                   name="currentPassword"
                                                   value="${user.password}"/>
                                        </div>
                                        <p style="color: #828282">Enter your current password</p>
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="text" class="form-control" id="email" name="email"
                                                   value="${user.email}"/>
                                        </div>
                                        <p style="color: #828282">A valid email adress</p>

                                        <div class="form-group">
                                            <label for="txtNewPassword">New Password</label>
                                            <input type="password" class="form-control" id="txtNewPassword"
                                                   name="newPassword"/>
                                        </div>

                                        <div class="form-group">
                                            <label for="txtConfirmPassword">Confirm Password</label>
                                            <input type="password" class="form-control" id="txtConfirmPassword"/>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Create new account</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Order Information -->

                    <c:if test="${classActiveOrder==true}">
                        <div class="tab-pane fade active" id="tab-2">
                            <div class="card-group">
                                <div class="card" style="border: none;">
                                    <div class="card-body">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:if>


                    <!-- Billing Information -->

                    <c:choose>
                    <c:when test="${classActiveBilling==true}">
                    <div class="tab-pane fade show active" id="tab-3">
                        </c:when>
                        <c:otherwise>
                        <div class="tab-pane fade" id="tab-3">
                            </c:otherwise>
                            </c:choose>

                            <div class="card-group">
                                <div class="card" style="border: none;">
                                    <div class="card-body">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item active">
                                                <a href="/listOfCreditCard">List of Credit Cards</a>
                                            </li>
                                            <li class="breadcrumb-item active">
                                                <a href="/addNewCreditCard">Add(Update) Credit Card</a>
                                            </li>
                                        </ol>


                                        <c:if test="${listOfCreditCards==true}">
                                            <form action="/setDefaultPayment" method="post">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th>Default</th>
                                                        <th>Credit Card</th>
                                                        <th>Operations</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    <c:forEach items="${userPaymentList}" var="userPayment">
                                                        <tr>
                                                            <td>
                                                                <c:choose>
                                                                    <c:when test="${userPayment.defaultPayment==true}">
                                                                        <input type="radio" name="defaultUserPaymentId"
                                                                               checked="checked"
                                                                               value="${userPayment.id}"/>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <input type="radio" name="defaultUserPaymentId"
                                                                               value="${userPayment.id}"/>
                                                                    </c:otherwise>
                                                                </c:choose>

                                                            </td>
                                                            <td>${userPayment.cardName}</td>
                                                            <td>
                                                                <a href="/updateCreditCard?id=${userPayment.id}">Update
                                                                    <i class="fa fa-pencil"></i></a>
                                                                <a href="/removeCreditCard?id=${userPayment.id}">Remove
                                                                    <i class="fa fa-times"></i></a>
                                                            </td>
                                                        </tr>
                                                    </c:forEach>


                                                    </tbody>
                                                </table>
                                                <button class="btn btn-primary" type="submit">Save</button>
                                            </form>
                                        </c:if>


                                        <c:if test="${addNewCreditCard == true}">
                                                <form action="addNewCreditCard" method="post">
                                                    <c:if test="${updateUserPaymentInfo == true}">
                                                        <div class="bg-info">User info updated.</div>
                                                    </c:if>

                                                    <input hidden="hidden" name="id" value="${userPayment.id}">

                                                    <div class="form-group">
                                                        <h5>* Give a name for your card:</h5>
                                                        <input type="text" class="form-control" id="cardName"
                                                               placeHolder="Card Name" name="cardName"
                                                               required="required" value="${userPayment.cardName}"/>
                                                    </div>

                                                    <!-- Billing Address -->
                                                    <hr/>
                                                    <div class="form-group">
                                                        <h4>Billing Address</h4>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="billingName">* Name</label> <input type="text"
                                                                                                       class="form-control"
                                                                                                       id="billingName"
                                                                                                       placeHolder="Receiver Name"
                                                                                                       name="userBillingName"
                                                                                                       required="required"
                                                                                                       value="${userBilling.userBillingName}"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="baaa">* Street Address</label> <input
                                                            type="text" class="form-control" id="baaa"
                                                            placeHolder="Street Address 1" name="userBillingStreet1"
                                                            required="required"
                                                            value="${userBilling.userBillingStreet1}"/> <input
                                                            type="text" class="form-control" id="ba"
                                                            placeHolder="Street Address 2" name="userBillingStreet2"
                                                            value="${userBilling.userBillingStreet2}"/>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-xs-4">
                                                            <div class="form-group">
                                                                <label for="billingCity">* City</label> <input
                                                                    type="text"
                                                                    class="form-control"
                                                                    id="billingCity"
                                                                    placeHolder="Billing city"
                                                                    name="userBillingCity"
                                                                    required="required"
                                                                    value="${userBilling.userBillingCity}"/>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-4">
                                                            <div class="form-group">
                                                                <label for="billingState">* State</label>
                                                                <select
                                                                        id="billingState" class="form-control"
                                                                        name="userBillingState"
                                                                        value="${userBilling.userBillingState}"
                                                                        required="required">
                                                                    <option value="" disabled="disabled">Please
                                                                        select an option
                                                                    </option>
                                                                    <c:forEach items="${stateList}" var="state">
                                                                        <c:choose>
                                                                            <c:when test="${userBilling.userBillingState==state}">
                                                                                <option selected="selected"
                                                                                        value="${state}">${state}</option>
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <option value="${state}">${state}</option>
                                                                            </c:otherwise>
                                                                        </c:choose>

                                                                    </c:forEach>

                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-4">
                                                            <div class="form-group">
                                                                <label for="billingZipcode">* Zipcode</label> <input
                                                                    type="text" class="form-control" id="billingZipcode"
                                                                    placeHolder="Billing Zipcode"
                                                                    name="userBillingZipcode"
                                                                    required="required"
                                                                    value="${userBilling.userBillingZipcode}"/>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <!-- Credit Card Information -->
                                                    <hr/>
                                                    <div class="form-group">
                                                        <h4>Credit Card Information</h4>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <img src="../static/image/creditcard.png"
                                                                 class="img-responsive"/><br/>
                                                            <div class="form-group">
                                                                <label for="cardType">* Select Card Type:</label>
                                                                <select class="form-control" id="cardType" name="type"
                                                                        value="${userPayment.type}">
                                                                    <c:choose>
                                                                        <c:when test="${userPayment.type=='visa'}">
                                                                            <option selected="selected" value="visa">
                                                                                Visa
                                                                            </option>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <option value="visa">Visa</option>
                                                                        </c:otherwise>
                                                                    </c:choose>

                                                                    <c:choose>
                                                                        <c:when test="${userPayment.type=='mastercard'}">
                                                                            <option selected="selected"
                                                                                    value="mastercard">Mastercard
                                                                            </option>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <option value="mastercard">Mastercard
                                                                            </option>
                                                                        </c:otherwise>
                                                                    </c:choose>

                                                                    <c:choose>
                                                                        <c:when test="${userPayment.type=='discover'}">
                                                                            <option selected="selected"
                                                                                    value="discover">Discover
                                                                            </option>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <option value="discover">Discover</option>
                                                                        </c:otherwise>
                                                                    </c:choose>

                                                                    <c:choose>
                                                                        <c:when test="${userPayment.type=='amex'}">
                                                                            <option selected="selected" value="amex">
                                                                                American Express
                                                                            </option>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <option value="amex">American Express
                                                                            </option>
                                                                        </c:otherwise>
                                                                    </c:choose>


                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="cardHolder">* Card Holder Name:</label>
                                                                <input type="text" class="form-control" id="cardHolder"
                                                                       required="required"
                                                                       placeHolder="Card Holder Name" name="holderName"
                                                                       value="${userPayment.holderName}"/>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="cardNumber">* Card Number:</label>
                                                                <div class="input-group">
                                                                    <input type="tel" class="form-control"
                                                                           id="cardNumber"
                                                                           required="required"
                                                                           placeHolder="Valid Card Number"
                                                                           name="cardNumber"
                                                                           value="${userPayment.cardNumber}"/>
                                                                    <span class="input-group-addon"><i
                                                                            class="fa fa-credit-card"
                                                                            aria-hidden="true"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-xs-7">
                                                            <div class="form-group">
                                                                <label>* Expiration Date</label>
                                                                <div class="row">
                                                                    <div class="col-xs-6">
                                                                        <select class="form-control" name="expiryMonth"
                                                                                required="required"
                                                                                value="${userPayment.expiryMonth}">
                                                                            <option disabled="disabled">-- Month --
                                                                            </option>
                                                                            <c:forEach begin="1" end="12" var="month">
                                                                                <c:choose>
                                                                                    <c:when test="${userPayment.expiryMonth==month}">
                                                                                        <option selected="selected"
                                                                                                value="${month}">${month}</option>
                                                                                    </c:when>
                                                                                    <c:otherwise>
                                                                                        <option value="${month}">${month}</option>
                                                                                    </c:otherwise>
                                                                                </c:choose>
                                                                            </c:forEach>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <select class="form-control" name="expiryYear"
                                                                                value="${userPayment.expiryYear}">
                                                                            <option disabled="disabled">-- Year --
                                                                            </option>

                                                                            <c:forEach begin="2017" end="2030"
                                                                                       var="year">
                                                                                <c:choose>
                                                                                    <c:when test="${userPayment.expiryYear==year}">
                                                                                        <option selected="selected"
                                                                                                value="${year}">${year}</option>
                                                                                    </c:when>
                                                                                    <c:otherwise>
                                                                                        <option value="${year}">${year}</option>
                                                                                    </c:otherwise>
                                                                                </c:choose>

                                                                            </c:forEach>

                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-5">
                                                            <div class="form-group">
                                                                <label for="phone">Enter a phone number:</label>
                                                                <input type="tel" id="phone" name="cvc"
                                                                       placeholder="050-555-55-55"
                                                                       pattern="[0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{2}"
                                                                       value="${userPayment.cvc}"/>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr/>
                                                    <button type="submit" class="btn btn-primary btn-lg">Save All
                                                    </button>
                                                </form>
                                        </c:if>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Shipping Information -->
                        <c:choose>
                        <c:when test="${classActiveShipping==true}">
                        <div class="tab-pane fade show active" id="tab-4">
                            </c:when>
                            <c:otherwise>
                            <div class="tab-pane fade" id="tab-4">
                                </c:otherwise>
                                </c:choose>


                                <div class="card-group">
                                    <div class="card" style="border: none;">
                                        <div class="card-body">

                                            <ol class="breadcrumb">
                                                <li class="breadcrumb-item active"><a
                                                        href="/listOfShippingAddresses">List of Shipping Addresses</a>
                                                </li>
                                                <li class="breadcrumb-item active"><a
                                                        href="/addNewShippingAddress">Add Shipping Address</a></li>
                                            </ol>

                                            <c:if test="${listOfShippingAddresses}">
                                                <form action="/setDefaultShippingAddress" method="post">
                                                    <table class="table">
                                                        <thead>
                                                        <tr>
                                                            <th>Default</th>
                                                            <th>Shipping Address</th>
                                                            <th>Operations</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <c:forEach items="${userShippingList}" var="userShipping">
                                                            <tr>
                                                                <td>
                                                                    <c:choose>
                                                                        <c:when test="${userShipping.userShippingDefault==true}">
                                                                            <input type="radio"
                                                                                   name="defaultShippingAddressId"
                                                                                   checked="checked"
                                                                                   value="${userShipping.id}"/>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <input type="radio"
                                                                                   name="defaultShippingAddressId"
                                                                                   value="${userShipping.id}"/>
                                                                        </c:otherwise>
                                                                    </c:choose>

                                                                </td>
                                                                <td>${userShipping.userShippingStreet1},
                                                                        ${userShipping.userShippingCity},
                                                                        ${userShipping.userShippingState}
                                                                </td>
                                                                <td>
                                                                    <a href="/updateUserShipping?id=${userShipping.id}">Update
                                                                        <i class="fa fa-pencil"></i></a>
                                                                    <a href="/removeUserShipping?id=${userShipping.id}">Remove
                                                                        <i class="fa fa-times"></i></a>
                                                                </td>
                                                            </tr>
                                                        </c:forEach>
                                                        </tbody>
                                                    </table>
                                                    <button class="btn btn-primary" type="submit">Save</button>
                                                </form>
                                            </c:if>


                                            <c:if test="${addNewShippingAddress==true}">

                                                <form action="addNewShippingAddress" method="post">
                                                    <c:if test="${updateUserShippingInfo==true}">
                                                        User info updated.
                                                    </c:if>

                                                    <input hidden="hidden" name="id" value="${userShipping.id}"/>

                                                    <!-- Shipping Address -->
                                                    <hr/>
                                                    <div class="form-group">
                                                        <h4>Shipping Address</h4>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="shippingName">* Name</label> <input type="text"
                                                                                                        class="form-control"
                                                                                                        id="shippingName"
                                                                                                        placeholder="Receiver Name"
                                                                                                        name="userShippingName"
                                                                                                        required="required"
                                                                                                        value="${userShipping.userShippingName}"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="shippingAddress">* Street Address</label> <input
                                                            type="text" class="form-control" id="shippingAddress"
                                                            placeholder="Street Address 1" name="userShippingStreet1"
                                                            required="required"
                                                            value="${userShipping.userShippingStreet1}"/> <input
                                                            type="text" class="form-control"
                                                            placeholder="Street Address 2" name="userShippingStreet2"
                                                            value="${userShipping.userShippingStreet2}"/>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-xs-4">
                                                            <div class="form-group">
                                                                <label for="shippingCity">* City</label> <input
                                                                    type="text"
                                                                    class="form-control" id="shippingCity"
                                                                    placeholder="Shipping City" name="userShippingCity"
                                                                    required="required"
                                                                    value="${userShipping.userShippingCity}"/>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-4">
                                                            <div class="form-group">
                                                                <label for="shippingState">* State</label> <select
                                                                    id="shippingState" class="form-control"
                                                                    name="userShippingState"
                                                                    value="${userShipping.userShippingState}"
                                                                    required="required">
                                                                <option value="" disabled="disabled">Please
                                                                    select an option
                                                                </option>
                                                                <c:forEach items="${stateList}" var="state">
                                                                    <c:choose>
                                                                        <c:when test="${userShipping.userShippingState==state}">
                                                                            <option selected="selected"
                                                                                    value="${state}">${state}</option>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <option value="${state}">${state}</option>
                                                                        </c:otherwise>
                                                                    </c:choose>

                                                                </c:forEach>
                                                            </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-4">
                                                            <div class="form-group">
                                                                <label for="shippingZipcode">* Zipcode</label> <input
                                                                    type="text" class="form-control"
                                                                    id="shippingZipcode"
                                                                    placeholder="Shipping Zipcode"
                                                                    name="userShippingZipcode" required="required"
                                                                    value="${userShipping.userShippingZipcode}"/>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <hr/>
                                                    <button type="submit" class="btn btn-primary btn-lg">Save
                                                        All
                                                    </button>
                                                </form>

                                            </c:if>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


</body>
</html>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!doctype html>
<html lang="en">
<head>
    <jsp:include page="../common/meta.jsp"/>
</head>

<body>
<jsp:include page="../common/header.jsp"/>

<div class="container box">

    <!--carousel-->
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100"
                     src="https://www.amrita.edu/sites/default/files/styles/news_banner_imagesbreakpoints_theme_norma_large_1x/public/fountain-pen-blog.jpg?itok=RTqZXXUs&timestamp=1548828364"
                     width="300" height="350"
                     alt="First slide"
                />
                <div class="carousel-caption">
                    <h4>First Label</h4>
                    <p>Pen</p>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100"
                     src="https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/beautiful-book-royalty-free-image-865109088-1543259326.jpg?crop=1.00xw:0.752xh;0,0.245xh&resize=768:*"
                     width="300" height="350"
                     alt="Second slide"/>
                <div class="carousel-caption">
                    <h4>Second Label</h4>
                    <p>Book</p>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100"
                     src="http://static.nautil.us/16493_945bcf80a53881dafde56a1aef8f627e.jpg"
                     width="300" height="350"
                     alt="Third slide"/>
                <div class="carousel-caption">
                    <h4>Third Label</h4>
                    <p>Binary</p>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
           data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button"
           data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>


    </div>
    <hr/>

    <div class="row">
        <div class="col-xs-4">
            <img src="../static/image/qaraqan.jpg" width="300" height="200" class="img-responsive">
        </div>
        <div class="col-xs-4">
            <img src="../static/image/eminem.jpg" width="300" height="200" class="img-responsive">
        </div>
        <div class="col-xs-4">
            <img src="../static/image/hakan.jpg" width="300" height="200" class="img-responsive">
        </div>
    </div>

    <div class="home-headline">
        <span>Featured Books</span>
        <hr style="margin-top: -15px; "/>
    </div>

    <div class="row">
        <div id="featured-books" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="row">
                        <div class="col-xl-2"><img class="img-responsive" src="../static/image/quantum.jpg" width="200"
                                                   height="100"/></div>
                        <div class="col-xl-2"><img class="img-responsive" src="../static/image/quantum.jpg" width="200"
                                                   height="100"/></div>
                        <div class="col-xl-2"><img class="img-responsive" src="../static/image/quantum.jpg" width="200"
                                                   height="100"/></div>
                        <div class="col-xl-2"><img class="img-responsive" src="../static/image/quantum.jpg" width="200"
                                                   height="100"/></div>

                    </div>

                </div>

                <div class="carousel-item">
                    <div class="row">
                        <div class="col-xl-2"><img class="img-responsive" src="../static/image/quantum.jpg" width="200"
                                                   height="100"/></div>
                        <div class="col-xl-2"><img class="img-responsive" src="../static/image/quantum.jpg" width="200"
                                                   height="100"/></div>
                        <div class="col-xl-2"><img class="img-responsive" src="../static/image/quantum.jpg" width="200"
                                                   height="100"/></div>
                        <div class="col-xl-2"><img class="img-responsive" src="../static/image/quantum.jpg" width="200"
                                                   height="100"/></div>

                    </div>

                </div>
                <div class="carousel-item">
                    <div class="row">
                        <div class="col-xl-2"><img class="img-responsive" src="../static/image/quantum.jpg" width="200"
                                                   height="100"/></div>
                        <div class="col-xl-2"><img class="img-responsive" src="../static/image/quantum.jpg" width="200"
                                                   height="100"/></div>
                        <div class="col-xl-2"><img class="img-responsive" src="../static/image/quantum.jpg" width="200"
                                                   height="100"/></div>
                        <div class="col-xl-2"><img class="img-responsive" src="../static/image/quantum.jpg" width="200"
                                                   height="100"/></div>

                    </div>

                </div>
            </div>
            <a class="carousel-control-prev" href="#featured-books" role="button"
               data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#featured-books" role="button"
               data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>


        </div>
    </div>

</div>



</body>
</html>

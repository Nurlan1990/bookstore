<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!doctype html>
<html lang="en">
<head>
    <jsp:include page="../common/meta.jsp"/>
</head>

<body>
<jsp:include page="../common/header.jsp"/>

<div class="container">
    <div class="row">
        <div class="col-xl-8">
            <h2 class="section-headline"><span>User Account</span></h2>
        </div>
        <div class="col-xl-4">
            <img src="../static/image/rap.jpg" class="img-responsive" width="200" height="200"/>
        </div>
        <hr/>
        <img src="../static/image/wood.jpeg" class="img-responsive"/>

        <div class="row">
            <div class="col-xl-12">
                <ul class="nav nav-tabs">
                    <li><a href="#tab-1" data-toggle="tab" class="nav-link"><span
                            style="color:red;">Create new account</span></a></li>&nbsp;

                    <c:choose>
                        <c:when test="${classActiveLogin==true}">
                            <li><a href="#tab-2" data-toggle="tab" class="nav-link active"><span style="color:red;">Login</span></a>
                            </li>
                            &nbsp;
                        </c:when>
                        <c:otherwise>
                            <li><a href="#tab-2" data-toggle="tab" class="nav-link"><span
                                    style="color:red;">Login</span></a></li>
                            &nbsp;
                        </c:otherwise>
                    </c:choose>

                    <c:choose>
                        <c:when test="${forgetPasswordEmailSend==true}">
                            <li><a href="#tab-3" data-toggle="tab" class="nav-link active"><span
                                    style="color:red;">Forget Password</span></a></li>
                            &nbsp;
                            &nbsp;
                        </c:when>
                        <c:otherwise>
                            <li><a href="#tab-3" data-toggle="tab" class="nav-link"><span
                                    style="color:red;">Forget Password</span></a></li>
                            &nbsp;
                            &nbsp;
                        </c:otherwise>
                    </c:choose>


                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade" id="tab-1">
                        <div class="card-group">
                            <div class="card" style="border: none;">
                                <div class="card-body">
                                    <c:if test="${emailSend == true}">
                                        <div class="alert alert-info">
                                            You have received the email from us. You got your password and account
                                            validation
                                            link. Pls click the link for activate your account.
                                        </div>
                                    </c:if>


                                    <form action="newUser" method="post">
                                        <div class="form-group">
                                            <label for="newUsername">* Username: </label>
                                            <input class="form-control" required="required" type="text" id="newUsername"
                                                   name="username"/>
                                            <p style="color: #828282">Enter your username here</p>
                                            <c:if test="${usernameExist == true}">
                                                <div class="alert alert-info">
                                                    This username already exist
                                                </div>
                                            </c:if>
                                        </div>
                                        <div class="form-group">
                                            <label for="email">* Email adress:</label>
                                            <input class="form-control" required="required" type="email" id="email"
                                                   name="email"/>
                                            <p style="color: #828282">A valid email addreess</p>
                                        </div>
                                        <c:if test="${emailExist == true}">
                                            <div class="alert alert-info">
                                                This email already exist
                                            </div>
                                        </c:if>

                                        <button type="submit" class="btn btn-primary">Create new account</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <c:choose>
                    <c:when test="${classActiveLogin==true}">
                    <div class="tab-pane fade show active" id="tab-2">
                        </c:when>
                        <c:otherwise>
                        <div class="tab-pane fade" id="tab-2">
                            </c:otherwise>
                            </c:choose>

                            <div class="card-group">
                                <div class="card" style="border: none;">
                                    <div class="card-body">
                                        <form action="login" method="post">
                                            <c:if test="${param.error != null}">
                                                <div class="alert alert-info">
                                                    Username or password is wrong
                                                </div>
                                            </c:if>
                                            <div class="form-group">
                                                <label for="username">* Username: </label>
                                                <input class="form-control" required="required" type="text"
                                                       id="username"
                                                       name="username"/>
                                                <p style="color: #828282">Enter your username here</p>
                                            </div>
                                            <div class="form-group">
                                                <label for="password">* Password:</label>
                                                <input class="form-control" required="required" type="password"
                                                       id="password"
                                                       name="password"/>
                                                <p style="color: #828282">Enter your password</p>
                                            </div>

                                            <button type="submit" class="btn btn-primary">Log in</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <c:choose>
                        <c:when test="${forgetPasswordEmailSend==true}">
                        <div class="tab-pane fade show active" id="tab-3">
                            </c:when>
                            <c:otherwise>
                            <div class="tab-pane fade" id="tab-3">
                                </c:otherwise>
                                </c:choose>

                                <div class="card-group">
                                    <div class="card" style="border: none;">
                                        <div class="card-body">
                                            <c:choose>
                                                <c:when test="${UserDoentExist==true}">
                                                    <div class="alert alert-danger">
                                                        Email does not exist
                                                    </div>
                                                </c:when>
                                                <c:otherwise>
                                                    <div class="alert alert-success">
                                                        Email send
                                                    </div>
                                                </c:otherwise>
                                            </c:choose>

                                            <form action="forgetPassword" method="post">
                                                <div class="form-group">
                                                    <label for="recoverEmail">* Email: </label>
                                                    <input class="form-control" required="required" type="email"
                                                           id="recoverEmail"
                                                           name="email"/>
                                                    <p style="color: #828282">Enter your email here</p>
                                                </div>

                                                <button type="submit" class="btn btn-primary">Submit</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>


</body>
</html>

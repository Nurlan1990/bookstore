<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="../common/meta.jsp"/>
</head>

<body>
<jsp:include page="../common/header.jsp"/>

<div class="container">
    <%--    <div class="row" style="margin-bottom: -100px;">--%>
    <%--        <div class="col-xs-8">--%>
    <%--            <h2 class="section-headline">--%>
    <%--                <h2 class="section-headline">--%>
    <%--                    <span th:text="${user}? ${#strings.toUpperCase(user.username)} : 'All Books'"></span>--%>
    <%--                </h2>--%>
    <%--            </h2>--%>
    <%--        </div>--%>
    <%--        <div class="col-xs-4">--%>
    <%--            <img src="/image/logo.png" class="img-responsive"/>--%>
    <%--        </div>--%>
    <%--    </div>--%>
    <%--    <hr--%>
    <%--            style="position: absolute; width: 100%; height: 6px; background-color: #333; z-index: -1; margin-top: -80px;"/>--%>
    <%--    <img class="img-responsive" src="/image/wood.png"--%>
    <%--         style="margin-top: -75px;"/>--%>

    <div class="row" style="margin-top: 60px;">
        <div class="col-xl-9 col-xl-offset-3">
            <c:if test="${emptyList == true}">
                <h5 style="font-style: italic;">Oops, no result is found. Try something else or try again later.</h5>
            </c:if>

            <table border="0" id="bookList">
                <thead>
                <tr>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${bookList}" var="book">
                    <tr class="book-item">
                        <td>
                            <c:if test="${book != null}">
                                <div class="row">
                                    <div class="col-xl-3">
                                       <a href="/bookDetail?id=${book.id}"> <img class="img-responsive shelf-book"
                                             src="../static/image/${book.id}.png" width="50" height="50"/>
                                       </a>
                                    </div>
                                    <div class="col-xl-9">
                                        <a href="/bookDetail?id=${book.id}"> <h4>${book.title}</h4></a>
                                        <span>${book.publicationDate}</span>
                                        <span>${book.author}</span>
                                        <a href="/bookDetail?id=${book.id}"><span>${book.format}</span></a>
                                        <span>${book.numberOfPages} pages</span>
                                        <a href="/bookDetail?id=${book.id}"><span style="font-size:x-large;color:#db3208;">${book.ourPrice}</span></a>
                                        <span style="text-decoration: line-through;">${book.listPrice}</span>
                                        <p>${book.description}</p>
                                    </div>
                                </div>
                            </c:if>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>


        </div>
    </div>
</div>
<!-- end of container -->


</body>
</html>

<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 04-Dec-20
  Time: 11:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="../common/meta.jsp"/>
</head>

<body>
<jsp:include page="../common/header.jsp"/>

<div class="container">
    <div class="row" style="margin-bottom: -100px;">
        <div class="col-xl-8">
            <h2 class="section-headline">

                <h2 class="section-headline">
                    <span>All Books</span>
                </h2>

            </h2>
        </div>
        <div class="col-xl-4">
            <img src="../static/image/rap.jpg" class="img-responsive" width="200" height="200"/>
        </div>
    </div>
    <hr style="position: absolute; width: 100%; height: 6px; background-color: #333; z-index: -1; margin-top: -80px;"/>
    <img src="../static/image/wood.jpeg" class="img-responsive"/>

    <div class="row" style="margin-top: 20px;">
        <div class="col-xl-12">
            <div class="row">
                <div class="col-xl-6 text-left">
                    <a class="btn btn-primary" href="/bookshelf">Continue Shopping</a>
                </div>
                <div class="col-xl-6 text-right">
                    <a class="btn btn-primary"
                       href="/checkout?id=${shoppingCart.id}">Check Out</a>
                </div>
                <br/>
                <c:if test="${notEnoughStock}">
                    <div class="alert alert-warning">
                        Oops, some of the products don't have enough stock. Please update
                        product quantity.
                    </div>
                </c:if>
                <c:if test="${emptyCart}">
                    <div class="alert alert-warning">Oops,
                        your cart is empty. See if you can find what you like in the
                        bookshelf and add them to cart.
                    </div>
                </c:if>
             <br/><br/>
                <div class="container">
                    <div class="row">
                        <div class="col-sm">
                            Products
                        </div>
                        <div class="col-sm">
                            Price
                        </div>
                        <div class="col-sm">
                            Qty
                        </div>
                    </div>
                </div>

                <!--**************** display products in cart ****************-->
                <c:forEach items="${cartItemList}" var="cartItem">
                    <div class="container">
                        <div class="row">
                            <form action="/shoppingCart/updateCartItem" method="post">
                                <hr/>
                                <div class="col-sm">
                                    <a href="bookDetail?id=${cartItem.book.id}"> <img
                                            class="img-responsive shelf-book"
                                            src="../static/image/${cartItem.book.id}.png" width="70" height="70"/>
                                    </a>

                                    <c:if test="${cartItem.book.stockNumber>=10}">
                                        <h4 style="color: green">${cartItem.book.stockNumber} In Stock</h4>
                                    </c:if>
                                    <c:if test="${cartItem.book.stockNumber<10 && cartItem.book.stockNumber>0}">
                                        <h4 style="color: green">Only <span>${cartItem.book.stockNumber} In Stock</span>
                                        </h4>
                                    </c:if>
                                    <c:if test="${cartItem.book.stockNumber==0}">
                                        <h4 style="color:darkred;">Product Unavailable</h4>&nbsp;
                                    </c:if>

                                </div>

                                <div class="col-sm">
                                    <h5 style="color: #db3208; font-size: large;">
                                        $<span>${cartItem.book.ourPrice}</span>
                                    </h5>
                                </div>

                                <div class="col-sm">
                                    <input type="hidden" name="id" value="${cartItem.id}" />
                                    <input type="number" id="${cartItem.id}" name="qty"

                                           class="form-control cartItemQty" value="${cartItem.qty}" />
                                    <button id="${cartItem.id}" type="submit"
                                            class="btn btn-warning btn-xs">Update
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </c:forEach>
                <div class="container">
                    <div class="row">
                        <hr/>
                        <h4 class="col-md-12 text-right">
                            <strong style="font-size: large;">Total Price (<span>${cartItemListSize}</span> items):
                            </strong> <span style="color: #db3208; font-szie: large;">$<span>${shoppingCart.grandTotal}</span></span>
                        </h4>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
<!-- end of container -->

</body>
</html>


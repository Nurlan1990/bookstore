<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 04-Nov-20
  Time: 15:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="page-top" style="width: 100%; height: 20px; background-color: #778899"></div>

<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light rounded navbar-dark bg-dark">
        <a class="navbar-brand" href="#">Bookstore</a>


        <div id="navbarsExample09">
            <ul class="navbar-nav mr-auto navbar-left">


                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="/bookSheif" id="dropdown09" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">BOOKS</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown09">
                        <a class="dropdown-item" href="/bookSheif">Browse the bookshelf</a>
                        <a class="dropdown-item" href="#">Store hours &#38; Directions</a>
                        <a class="dropdown-item" href="#">FAQ</a>
                    </div>
                </li>
                <form class="navbar-form">
                    <div class="form-group">
                        <input class="form-control" name="keyword" type="text" placeholder="Book title"
                               aria-label="Search">
                        <button type="submit" class="btn btn-primary">Search</button>
                    </div>
                </form>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/shoppingCart/cart"> Shopping Card </a></li>&nbsp;
                    <li><a href="/myAccount"> My Account </a></li>&nbsp;

                    <c:if test="${(user != null) || (forShowLogout==true)}">
                        <li><a href="/logout"> Logout </a></li>&nbsp;
                    </c:if>

                </ul>
            </ul>



        </div>
    </nav>



</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="../static/js/bootstrap.min.js"></script>

package com.hp.util;

import com.hp.domain.User;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.security.SecureRandom;
import java.util.Random;

@Component
public class SecurityUtiliy {

    private static final String SALT="salt"; // salt szou diqqetle qorunmalidir

    @Bean
    public static BCryptPasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder(12, new SecureRandom(SALT.getBytes()));
    }

    @Bean
    public static String randomPassword(){
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVW1234567890";
        StringBuilder salt = new StringBuilder();
        Random random = new Random();

        while (salt.length()<18){
            int index = (int) (random.nextFloat()*SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));

        }

        String saltString = salt.toString();
        return saltString;
    }

}

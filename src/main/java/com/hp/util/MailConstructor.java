package com.hp.util;

import com.hp.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;

import java.util.Locale;
import java.util.Objects;

@Component
public class MailConstructor {

    @Autowired
    private Environment environment;

    public SimpleMailMessage constructResetTokenEmail(String contextPath, Locale locale, String token, User user, String password){
        String url = contextPath + "/newUser?token="+token;
        String message = "\n Pls click on this link to verify email and edit your personal information. Your password is \n"+password;
        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(user.getEmail());
        email.setSubject("Bookstore");
        email.setText(url+message);
        email.setFrom(Objects.requireNonNull(environment.getProperty("support.email")));

        return email;

    }
}

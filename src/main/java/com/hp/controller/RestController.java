package com.hp.controller;

import com.hp.domain.*;
import com.hp.service.*;
import com.hp.security.UserSecuritySevice;
import com.hp.util.MailConstructor;
import com.hp.util.SecurityUtiliy;
import com.hp.util.USConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.w3c.dom.html.HTMLParagraphElement;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.*;


@org.springframework.web.bind.annotation.RestController
@RequestMapping("/")
public class RestController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserSecuritySevice userSecuritySevice;

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private MailConstructor mailConstructor;


    @Autowired
    private BookService bookService;

    @Autowired
    private UserPaymentService userPaymentService;

    @Autowired
    private UserBillingService userBillingService;

    @Autowired
    private UserShippingService userShippingService;


    @GetMapping("/")
    public ModelAndView index() {

        ModelAndView modelAndView = new ModelAndView("web/index");

        return modelAndView;
    }

    @GetMapping("/myAccount")
    public ModelAndView myAccount() {

        ModelAndView modelAndView = new ModelAndView("web/myAccount");

        return modelAndView;
    }

    @GetMapping("/login")
    public ModelAndView login() {
        ModelAndView modelAndView = new ModelAndView("web/myAccount");


        modelAndView.addObject("classActiveLogin", true);
        return modelAndView;
    }

    @PostMapping("/newUser")
    public ModelAndView newUserPost(
            HttpServletRequest request,
            @ModelAttribute(name = "email") String userEmail,
            @ModelAttribute(name = "username") String username
    ) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("classActiveNewAccount", true);
        modelAndView.addObject("email", userEmail);
        modelAndView.addObject("username", username);

        if (userService.findByUsername(username) != null) {
            modelAndView.addObject("usernameExist", true);
            modelAndView.addObject("emailSend", false);
            modelAndView.setViewName("web/myAccount");
        } else if (userService.findByEmail(userEmail) != null) {
            modelAndView.addObject("emailExist", true);
            modelAndView.addObject("emailSend", false);
            modelAndView.setViewName("web/myAccount");
        } else {
            User user = new User();
            user.setUsername(username);
            user.setEmail(userEmail);

            String password = SecurityUtiliy.randomPassword();
            String encryptedPassword = SecurityUtiliy.passwordEncoder().encode(password);

            user.setPassword(encryptedPassword);

            Role role = new Role();
            role.setRoleId((long) 1);
            role.setName("ROLE_USER");
            Set<UserRole> userRoles = new HashSet<>();
            userRoles.add(new UserRole(user, role));

            userService.createUser(user, userRoles);

            String token = UUID.randomUUID().toString();
            userService.createPasswordResetTokenForUser(user, token);

            String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();

            System.out.println("appUrl = " + appUrl);
            SimpleMailMessage email
                    = mailConstructor.constructResetTokenEmail(appUrl, request.getLocale(), token, user, password);
            mailSender.send(email);

            modelAndView.addObject("emailSend", true);

            modelAndView.setViewName("web/myAccount");
        }

        return modelAndView;

    }

    @GetMapping("/newUser")
    public ModelAndView newUser(
            @RequestParam(name = "token") String token

    ) {
        ModelAndView modelAndView = new ModelAndView();

        PasswordResetToken passwordResetToken = userService.getPasswordResetToken(token);

        if (passwordResetToken == null) {
            String message = "invalid token";
            modelAndView.addObject("message", message);
            modelAndView.setViewName("web/badRequest");
        } else {
            User user = passwordResetToken.getUser();

            String username = user.getUsername();
            UserDetails userDetails = userSecuritySevice.loadUserByUsername(username);

            Authentication authentication
                    = new UsernamePasswordAuthenticationToken(userDetails, userDetails.getPassword(), userDetails.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(authentication);

            modelAndView.addObject("user", user);
            modelAndView.addObject("classActiveEdit", true);


            modelAndView.setViewName("web/myProfile");

        }
        return modelAndView;

    }

    @PostMapping("/forgetPassword")
    public ModelAndView forgetPassword(
            HttpServletRequest request,
            @ModelAttribute(name = "email") String email

    ) {
        ModelAndView modelAndView = new ModelAndView();

        User user = userService.findByEmail(email);

        if (user == null) {
            modelAndView.addObject("UserDoentExist", true);
            modelAndView.addObject("forgetPasswordEmailSend", true);
        } else {

            String password = SecurityUtiliy.randomPassword();
            String encryptedPassword = SecurityUtiliy.passwordEncoder().encode(password);

            user.setPassword(encryptedPassword);
            userService.seve(user);

            String token = UUID.randomUUID().toString();
            userService.createPasswordResetTokenForUser(user, token);

            String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();

            SimpleMailMessage newEmail
                    = mailConstructor.constructResetTokenEmail(appUrl, request.getLocale(), token, user, password);
            mailSender.send(newEmail);

            modelAndView.addObject("forgetPasswordEmailSend", true);

        }
        modelAndView.setViewName("web/myAccount");

        return modelAndView;

    }


    @GetMapping("/myProfile")
    public ModelAndView myProfile(HttpSession session) {
        ModelAndView modelAndView = new ModelAndView("web/myProfile");
        modelAndView.addObject("forShowLogout", true);

        User user = (User) session.getAttribute("user");

        List<UserPayment> userPaymentList
                = userPaymentService.getUserPaymentListById(user.getId());
        user.setUserPaymentList(userPaymentList);

        List<UserShipping> userShippingList
                = userShippingService.getUserShippingListById(user.getId());
        user.setUserShippingList(userShippingList);

        modelAndView.addObject("user", user);
        modelAndView.addObject("userPaymentList", user.getUserPaymentList());
        modelAndView.addObject("userShippingList", user.getUserShippingList());

        // modelAndView.addObject("orderList",user.getOrderList());

        UserShipping userShipping = new UserShipping();
        modelAndView.addObject("userShipping", userShipping);

        modelAndView.addObject("listOfCreditCards", true);
        modelAndView.addObject("listOfShippingAddresses", true);

        List<String> stateList = USConstants.listOfUSStatesCode;
        Collections.sort(stateList);
        modelAndView.addObject("stateList", stateList);
        modelAndView.addObject("classActiveEdit", true);

        return modelAndView;
    }

    @GetMapping("/listOfCreditCard")
    public ModelAndView listOfCreditCard(HttpSession session) {

        ModelAndView modelAndView = new ModelAndView("web/myProfile");

        User user = (User) session.getAttribute("user");

        List<UserPayment> userPaymentList = userPaymentService.getUserPaymentListById(user.getId());
        user.setUserPaymentList(userPaymentList);

        List<UserShipping> userShippingList
                = userShippingService.getUserShippingListById(user.getId());
        user.setUserShippingList(userShippingList);

        modelAndView.addObject("user", user);
        modelAndView.addObject("userPaymentList", user.getUserPaymentList());
        modelAndView.addObject("userShippingList", user.getUserShippingList());

        // modelAndView.addObject("orderList",user.getOrderList());

        modelAndView.addObject("listOfCreditCards", true);
        modelAndView.addObject("classActiveBilling", true);
        modelAndView.addObject("listOfShippingAddresses", true);


        return modelAndView;
    }

    @GetMapping("/listOfShippingAddresses")
    public ModelAndView listOfShippingAddresses(HttpSession session) {

        ModelAndView modelAndView = new ModelAndView("web/myProfile");

        User user = (User) session.getAttribute("user");

        List<UserPayment> userPaymentList = userPaymentService.getUserPaymentListById(user.getId());
        user.setUserPaymentList(userPaymentList);

        List<UserShipping> userShippingList
                = userShippingService.getUserShippingListById(user.getId());
        user.setUserShippingList(userShippingList);


        modelAndView.addObject("user", user);
        modelAndView.addObject("userPaymentList", user.getUserPaymentList());
        modelAndView.addObject("userShippingList", user.getUserShippingList());

        // modelAndView.addObject("orderList",user.getOrderList());

        modelAndView.addObject("classActiveShipping", true);
        modelAndView.addObject("listOfShippingAddresses", true);


        return modelAndView;
    }

    @GetMapping("/addNewCreditCard")
    public ModelAndView addNewCreditCard(HttpSession session) {

        ModelAndView modelAndView = new ModelAndView("web/myProfile");

        User user = (User) session.getAttribute("user");


        List<UserPayment> userPaymentList = userPaymentService.getUserPaymentListById(user.getId());
        user.setUserPaymentList(userPaymentList);

        List<UserShipping> userShippingList
                = userShippingService.getUserShippingListById(user.getId());
        user.setUserShippingList(userShippingList);

        modelAndView.addObject("user", user);
        modelAndView.addObject("addNewCreditCard", true);
        modelAndView.addObject("classActiveBilling", true);

        UserBilling userBilling = new UserBilling();
        UserPayment userPayment = new UserPayment();
        modelAndView.addObject("userBilling", userBilling);
        modelAndView.addObject("userPayment", userPayment);

        List<String> stateList = USConstants.listOfUSStatesCode;
        Collections.sort(stateList);
        modelAndView.addObject("stateList", stateList);
        modelAndView.addObject("userPaymentList", user.getUserPaymentList());
        modelAndView.addObject("userShippingList", user.getUserShippingList());
        /*model.addAttribute("orderList", user.orderList());*/

        return modelAndView;
    }


    @Transactional
    @PostMapping("/addNewCreditCard")
    public ModelAndView addNewCreditCard(
            @ModelAttribute("userPayment") UserPayment userPayment,
            @ModelAttribute("userBilling") UserBilling userBilling,
            HttpSession session
    ) {
        ModelAndView modelAndView = new ModelAndView("web/myProfile");

        User user = (User) session.getAttribute("user");

        userPaymentService.updateUserBilling(userBilling, userPayment, user);

        List<UserPayment> userPaymentList
                = userPaymentService.getUserPaymentListById(user.getId());
        user.setUserPaymentList(userPaymentList);

        List<UserShipping> userShippingList
                = userShippingService.getUserShippingListById(user.getId());
        user.setUserShippingList(userShippingList);


        modelAndView.addObject("user", user);
        modelAndView.addObject("userPaymentList", user.getUserPaymentList());
        modelAndView.addObject("userShippingList", user.getUserShippingList());

        modelAndView.addObject("classActiveBilling", true);
        modelAndView.addObject("listOfCreditCards", true);

        return modelAndView;
    }

    @GetMapping("/updateCreditCard")
    public ModelAndView updateCreditCard(
            @ModelAttribute("id") Long creditCardId,
            HttpSession session
    ) {
        ModelAndView modelAndView = new ModelAndView();

        User user = (User) session.getAttribute("user");
        Optional<UserPayment> optionalUserPayment = userPaymentService.findById(creditCardId);
        UserPayment userPayment = optionalUserPayment.get();

        List<UserPayment> userPaymentList
                = userPaymentService.getUserPaymentListById(user.getId());
        user.setUserPaymentList(userPaymentList);

        List<UserShipping> userShippingList
                = userShippingService.getUserShippingListById(user.getId());
        user.setUserShippingList(userShippingList);

        if (!user.getId().equals(userPayment.getUser().getId())) {
            modelAndView.setViewName("web/badRequest");
        } else {
            modelAndView.addObject("user", user);
            UserBilling userBilling = userPayment.getUserBilling();
            modelAndView.addObject("userPayment", userPayment);
            modelAndView.addObject("userBilling", userBilling);

            List<String> stateList = USConstants.listOfUSStatesCode;
            Collections.sort(stateList);
            modelAndView.addObject("stateList", stateList);

            modelAndView.addObject("addNewCreditCard", true);
            modelAndView.addObject("classActiveBilling", true);

            modelAndView.addObject("userPaymentList", user.getUserPaymentList());
            modelAndView.addObject("userShippingList", user.getUserShippingList());
            modelAndView.setViewName("web/myProfile");

        }
        return modelAndView;
    }

    @GetMapping("/removeCreditCard")
    @Transactional
    public ModelAndView removeCreditCard(
            @ModelAttribute("id") Long creditCardId,
            HttpSession session
    ) {
        ModelAndView modelAndView = new ModelAndView();

        User user = (User) session.getAttribute("user");

        Optional<UserPayment> optionalUserPayment = userPaymentService.findById(creditCardId);
        UserPayment userPayment = optionalUserPayment.get();


        if (!user.getId().equals(userPayment.getUser().getId())) {
            modelAndView.setViewName("web/badRequest");
        } else {

            userBillingService.deleteUserBillingById(creditCardId);
            userPaymentService.deleteUserPaymentById(creditCardId);

            List<UserPayment> userPaymentList
                    = userPaymentService.getUserPaymentListById(user.getId());
            user.setUserPaymentList(userPaymentList);

            List<UserShipping> userShippingList
                    = userShippingService.getUserShippingListById(user.getId());
            user.setUserShippingList(userShippingList);

            modelAndView.addObject("user", user);
            modelAndView.addObject("listOfCreditCards", true);
            modelAndView.addObject("classActiveBilling", true);

            modelAndView.addObject("userPaymentList", user.getUserPaymentList());
            modelAndView.addObject("userShippingList", user.getUserShippingList());

            modelAndView.setViewName("web/myProfile");
        }

        return modelAndView;
    }

    @PostMapping("/setDefaultPayment")
    public ModelAndView setDefaultPayment(
            @ModelAttribute("defaultUserPaymentId") Long defaultPaymentId,
            HttpSession session
    ) {
        ModelAndView modelAndView = new ModelAndView("web/myProfile");

        User user = (User) session.getAttribute("user");
        userService.setUserDefaultPayment(defaultPaymentId, user);

        List<UserPayment> userPaymentList = userPaymentService.getUserPaymentListById(user.getId());
        user.setUserPaymentList(userPaymentList);

        List<UserShipping> userShippingList
                = userShippingService.getUserShippingListById(user.getId());
        user.setUserShippingList(userShippingList);


        modelAndView.addObject("user", user);
        modelAndView.addObject("listOfCreditCards", true);
        modelAndView.addObject("classActiveBilling", true);
        modelAndView.addObject("listOfShippingAddresses", true);

        modelAndView.addObject("userPaymentList", user.getUserPaymentList());
        modelAndView.addObject("userShippingList", user.getUserShippingList());

        return modelAndView;
    }

    @PostMapping("/setDefaultShippingAddress")
    public ModelAndView setDefaultShippingAddress(
            @ModelAttribute("defaultShippingAddressId") Long defaultShippingId,
            HttpSession session
    ) {
        ModelAndView modelAndView = new ModelAndView("web/myProfile");
        User user = (User) session.getAttribute("user");
        userService.setUserDefaultShipping(defaultShippingId, user);

        List<UserPayment> userPaymentList = userPaymentService.getUserPaymentListById(user.getId());
        user.setUserPaymentList(userPaymentList);

        List<UserShipping> userShippingList
                = userShippingService.getUserShippingListById(user.getId());
        user.setUserShippingList(userShippingList);

        modelAndView.addObject("user", user);
       // modelAndView.addObject("listOfCreditCards", true);
        modelAndView.addObject("classActiveShipping", true);
        modelAndView.addObject("listOfShippingAddresses", true);

        modelAndView.addObject("userPaymentList", user.getUserPaymentList());
        modelAndView.addObject("userShippingList", user.getUserShippingList());

        return modelAndView;
    }


    @GetMapping("/addNewShippingAddress")
    public ModelAndView addNewShippingAddress(HttpSession session) {

        ModelAndView modelAndView = new ModelAndView("web/myProfile");

        User user = (User) session.getAttribute("user");

        List<UserPayment> userPaymentList
                = userPaymentService.getUserPaymentListById(user.getId());
        user.setUserPaymentList(userPaymentList);

        List<UserShipping> userShippingList
                = userShippingService.getUserShippingListById(user.getId());
        user.setUserShippingList(userShippingList);

        modelAndView.addObject("user", user);
        modelAndView.addObject("addNewShippingAddress", true);
        modelAndView.addObject("classActiveShipping", true);
        modelAndView.addObject("listOfCreditCards", true);

        UserShipping userShipping = new UserShipping();

        modelAndView.addObject("userShipping", userShipping);

        List<String> stateList = USConstants.listOfUSStatesCode;
        Collections.sort(stateList);
        modelAndView.addObject("stateList", stateList);
        modelAndView.addObject("userPaymentList", user.getUserPaymentList());
        modelAndView.addObject("userShippingList", user.getUserShippingList());
        /*model.addAttribute("orderList", user.orderList());*/

        return modelAndView;
    }


    @PostMapping("/addNewShippingAddress")
    public ModelAndView addNewShippingAddressPost(
            @ModelAttribute("userShipping") UserShipping userShipping,
            HttpSession session
    ) {
        ModelAndView modelAndView = new ModelAndView("web/myProfile");

        User user = (User) session.getAttribute("user");

        userService.updateUserShipping(userShipping,user);


        List<UserPayment> userPaymentList
                = userPaymentService.getUserPaymentListById(user.getId());
        user.setUserPaymentList(userPaymentList);

        List<UserShipping> userShippingList
                = userShippingService.getUserShippingListById(user.getId());
        user.setUserShippingList(userShippingList);


        modelAndView.addObject("user", user);
        modelAndView.addObject("userPaymentList", user.getUserPaymentList());
        modelAndView.addObject("userShippingList", user.getUserShippingList());
        modelAndView.addObject("listOfShippingAddresses", true);
        modelAndView.addObject("classActiveShipping", true);
      //  modelAndView.addObject("listOfCreditCards", true);

        return modelAndView;
    }

    @GetMapping("/updateUserShipping")
    public ModelAndView updateUserShipping(
            @ModelAttribute("id") Long shippingAddressId,
            HttpSession session
    ) {
        ModelAndView modelAndView = new ModelAndView();

        User user = (User) session.getAttribute("user");
        Optional<UserShipping> optionalUserShipping = userShippingService.findById(shippingAddressId);
        UserShipping userShipping = optionalUserShipping.get();

        if (!user.getId().equals(userShipping.getUser().getId())) {
            modelAndView.setViewName("web/badRequest");
        } else {
            modelAndView.addObject("user", user);

            modelAndView.addObject("userShipping", userShipping);

            List<UserPayment> userPaymentList
                    = userPaymentService.getUserPaymentListById(user.getId());
            user.setUserPaymentList(userPaymentList);

            List<UserShipping> userShippingList
                    = userShippingService.getUserShippingListById(user.getId());
            user.setUserShippingList(userShippingList);

            List<String> stateList = USConstants.listOfUSStatesCode;
            Collections.sort(stateList);
            modelAndView.addObject("stateList", stateList);

          //  modelAndView.addObject("listOfShippingAddresses", true);
            modelAndView.addObject("classActiveShipping", true);
            modelAndView.addObject("addNewShippingAddress", true);

            modelAndView.addObject("userPaymentList", user.getUserPaymentList());
            modelAndView.addObject("userShippingList", user.getUserShippingList());

           modelAndView.setViewName("web/myProfile");
        }
        return modelAndView;
    }

    @Transactional
    @GetMapping("/removeUserShipping")
    public ModelAndView removeUserShipping(
            @ModelAttribute("id") Long userShippingId,
            HttpSession session
    ){
        ModelAndView modelAndView = new ModelAndView();

        User user = (User) session.getAttribute("user");
        Optional<UserShipping> optionalUserShipping = userShippingService.findById(userShippingId);
        UserShipping userShipping = optionalUserShipping.get();

        if(!user.getId().equals(userShipping.getUser().getId())) {
            modelAndView.setViewName("web/badRequest");
        } else {

            userShippingService.removeById(userShippingId);

            List<UserPayment> userPaymentList
                    = userPaymentService.getUserPaymentListById(user.getId());
            user.setUserPaymentList(userPaymentList);

            List<UserShipping> userShippingList
                    = userShippingService.getUserShippingListById(user.getId());
            user.setUserShippingList(userShippingList);

            modelAndView.addObject("user", user);

            modelAndView.addObject("listOfShippingAddresses", true);
            modelAndView.addObject("classActiveShipping", true);

            modelAndView.addObject("userPaymentList", user.getUserPaymentList());
            modelAndView.addObject("userShippingList", user.getUserShippingList());

            modelAndView.setViewName("web/myProfile");
        }
        return modelAndView;
    }



    @GetMapping("/bookSheif")
    public ModelAndView bookSheif() {
        ModelAndView modelAndView = new ModelAndView("web/bookSheif");

        List<Book> bookList = bookService.findAll();
        if (bookList == null) {
            modelAndView.addObject("emptyList", true);
        }
        modelAndView.addObject("bookList", bookList);

        return modelAndView;
    }

    @GetMapping("/bookDetail")
    public ModelAndView bookDetail(
            @RequestParam(name = "id") Long id,
            HttpSession session

    ) {
        ModelAndView modelAndView = new ModelAndView("web/bookDetail");

        User user = (User) session.getAttribute("user");
        modelAndView.addObject("user", user);

        Optional<Book> optionalBook = bookService.findById(id);
        if (optionalBook.isPresent()) {
            Book book = optionalBook.get();
            modelAndView.addObject("book", book);
        }

        List<Integer> qytList = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        modelAndView.addObject("qytList", qytList);


        return modelAndView;
    }


}












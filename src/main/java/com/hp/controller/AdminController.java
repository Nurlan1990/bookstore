package com.hp.controller;

import com.hp.domain.Book;
import com.hp.domain.DataTableRequest;
import com.hp.domain.DataTableResult;
import com.hp.domain.User;
import com.hp.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = {"/admin", "/admin/"})
public class AdminController {

    @Autowired
    private BookService bookService;

    @GetMapping("/")
    public ModelAndView homePage(HttpSession session) {
        ModelAndView modelAndView = new ModelAndView("index");
        User adminUser = (User) session.getAttribute("user");
        System.out.println("user admin = " + adminUser);

        return modelAndView;
    }

    @GetMapping("/addBook")
    public ModelAndView forwardtoAddBook() {
        ModelAndView modelAndView = new ModelAndView("addBook");
        Book book = new Book();

        modelAndView.addObject("book", book);

        return modelAndView;
    }

    @PostMapping("/addBook")
    public ModelAndView addBook(
            @ModelAttribute(name = "book") Book book
    ) {
        ModelAndView modelAndView = new ModelAndView("redirect:bookList");

        bookService.save(book);

        MultipartFile bookImage = book.getBookImage();

        try {
            byte[] bytes = bookImage.getBytes();
            String name = book.getId() + ".png";
            BufferedOutputStream stream
                    = new BufferedOutputStream(new FileOutputStream(
                    new File("src/main/webapp/static/image/" + name))
            );
            stream.write(bytes);
            stream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


        return modelAndView;
    }

    @GetMapping("/bookList")
    public ModelAndView bookList() {
        ModelAndView modelAndView = new ModelAndView("bookList");

        return modelAndView;
    }

    @GetMapping("/getBookList")
    @ResponseBody
    public DataTableResult getJobsByAjax(@RequestParam(name = "start") int start,
                                         @RequestParam(name = "length") int length,
                                         @RequestParam(name = "search[value]") String searchValue,
                                         @RequestParam(name = "order[0][column]") int column,
                                         @RequestParam(name = "order[0][dir]") String orderDir,
                                         @RequestParam(name = "draw") int draw) {

        DataTableRequest dataTableRequest = new DataTableRequest();
        dataTableRequest.setDraw(draw);
        dataTableRequest.setLength(length);
        dataTableRequest.setStart(start);
        dataTableRequest.setSortColumn(column);
        dataTableRequest.setSortDirection(orderDir);
        dataTableRequest.setFilter(searchValue);

        return bookService.getCompanyJobDataTableResult(dataTableRequest, start, length);
    }

    @GetMapping("/bookInfo")
    public ModelAndView bookInfo(
            @RequestParam(name = "id") Long id

    ) {
        ModelAndView modelAndView = new ModelAndView("bookInfo");


        Optional<Book> optionalBook = bookService.findById(id);
        if (optionalBook.isPresent()) {
            Book book = optionalBook.get();
            modelAndView.addObject("book", book);
        }

        return modelAndView;
    }


    @GetMapping("/updateBook")
    public ModelAndView updateBook(
            @RequestParam(name = "id") Long id

    ) {
        ModelAndView modelAndView = new ModelAndView("updateBook");


        Optional<Book> optionalBook = bookService.findById(id);
        if (optionalBook.isPresent()) {
            Book book = optionalBook.get();
            modelAndView.addObject("book", book);
        }

        return modelAndView;
    }


    @PostMapping("/updateBook")
    public ModelAndView updateBookPost(
            @ModelAttribute(name = "book") Book book

    ) {

        ModelAndView modelAndView = new ModelAndView("redirect:bookInfo?id=" + book.getId());

        bookService.save(book);


        MultipartFile bookImage = book.getBookImage();

        try {
            byte[] bytes = bookImage.getBytes();
            String name = book.getId() + ".png";


            Files.delete(Paths.get("src/main/webapp/static/image/" + name));


            BufferedOutputStream stream
                    = new BufferedOutputStream(new FileOutputStream(
                    new File("src/main/webapp/static/image/" + name))
            );
            stream.write(bytes);
            stream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


        return modelAndView;
    }


}











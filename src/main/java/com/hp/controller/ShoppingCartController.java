package com.hp.controller;


import java.security.Principal;
import java.util.List;
import java.util.Optional;

import com.hp.domain.Book;
import com.hp.service.BookService;
import com.hp.service.CartItemService;
import com.hp.service.ShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.hp.domain.CartItem;
import com.hp.domain.ShoppingCart;
import com.hp.domain.User;
import com.hp.service.UserService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/shoppingCart")
public class ShoppingCartController {

    @Autowired
    private UserService userService;

    @Autowired
    private CartItemService cartItemService;

    @Autowired
    private ShoppingCartService shoppingCartService;

    @Autowired
    private BookService bookService;

    @GetMapping("/cart")
    public ModelAndView shoppingCart(Principal principal) {
        ModelAndView modelAndView = new ModelAndView("shoppingCart");

        User user = userService.findByUsername(principal.getName());

        ShoppingCart shoppingCart = user.getShoppingCart();

        List<CartItem> cartItemList = cartItemService.findByShoppingCart(shoppingCart);

        shoppingCartService.updateShoppingCart(shoppingCart);

        modelAndView.addObject("cartItemList", cartItemList);
        modelAndView.addObject("shoppingCart", shoppingCart);

        return modelAndView;
    }

    @PostMapping("/addItem")
    public ModelAndView addItem(
            @ModelAttribute("book") Book book1,
            @ModelAttribute("qty") String qty,
            Principal principal
    ) {
        ModelAndView modelAndView = new ModelAndView();
        User user = userService.findByUsername(principal.getName());
        System.out.println("book="+book1);
        System.out.println("qty="+qty);

        Optional<Book> optionalBook = bookService.findById(book1.getId());
        Book book = optionalBook.get();

        if (Integer.parseInt(qty) > book.getStockNumber()) {
            modelAndView.addObject("notEnoughStock", true);
            String urlError=String.format("redirect:/bookDetail?id=%d", book.getId());

            modelAndView.setViewName(urlError);
        }

        CartItem cartItem = cartItemService.addBookToCartItem(book, user, Integer.parseInt(qty));
        modelAndView.addObject("addBookSuccess", true);

        String url = String.format("redirect:/bookDetail?id=%d", book.getId());

        modelAndView.setViewName(url);

        return modelAndView;
    }



}

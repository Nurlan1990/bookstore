package com.hp.service;

import com.hp.domain.*;

import java.util.Set;

public interface UserService {

    PasswordResetToken getPasswordResetToken(final String token);

    void createPasswordResetTokenForUser(final User user, final String token);

    User findByUsername(String username);
    User findByEmail(String email);

    User createUser(User user, Set<UserRole> userRoles);

    User seve(User user);

    void setUserDefaultPayment(Long userPaymentId, User user);


    void updateUserShipping(UserShipping userShipping, User user);

    void setUserDefaultShipping(Long defaultShippingId, User user);
}

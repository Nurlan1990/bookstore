package com.hp.service;

import com.hp.domain.User;
import com.hp.domain.UserBilling;
import com.hp.domain.UserPayment;

import java.util.List;
import java.util.Optional;

public interface UserPaymentService {
    Optional<UserPayment> findById(Long id);

    void deleteUserPaymentById(Long creditCardId);

    void updateUserBilling(UserBilling userBilling, UserPayment userPayment, User user);

    List<UserPayment> getUserPaymentListById(Long userId);
}

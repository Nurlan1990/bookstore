package com.hp.service;

import com.hp.domain.Book;
import com.hp.domain.CartItem;
import com.hp.domain.ShoppingCart;
import com.hp.domain.User;

import java.util.List;

public interface CartItemService {
    List<CartItem> findByShoppingCart(ShoppingCart shoppingCart);

    CartItem updateCartItem(CartItem cartItem);

    CartItem addBookToCartItem(Book book, User user, int qty);
}

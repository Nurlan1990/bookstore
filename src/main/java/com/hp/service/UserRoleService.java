package com.hp.service;

import com.hp.domain.Role;
import com.hp.domain.UserRole;

import java.util.List;
import java.util.Set;

public interface UserRoleService {


    Set<UserRole> findByUserId(Long id);

}

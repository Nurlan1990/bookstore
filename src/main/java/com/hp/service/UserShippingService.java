package com.hp.service;

import com.hp.domain.User;
import com.hp.domain.UserShipping;

import java.util.List;
import java.util.Optional;

public interface UserShippingService {
    List<UserShipping> getUserShippingListById(Long id);

    Optional<UserShipping> findById(Long shippingAddressId);


    void removeById(Long userShippingId);
}

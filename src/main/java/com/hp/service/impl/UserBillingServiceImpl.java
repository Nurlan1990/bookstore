package com.hp.service.impl;

import com.hp.domain.UserPayment;
import com.hp.repository.UserBillingRepo;
import com.hp.service.UserBillingService;
import com.hp.service.UserPaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class UserBillingServiceImpl implements UserBillingService {

    @Autowired
    private UserBillingRepo userBillingRepo;

    @Override
    @Transactional
    public void deleteUserBillingById(Long creditCardId) {
        userBillingRepo.deleteUserBillingById(creditCardId);
    }
}

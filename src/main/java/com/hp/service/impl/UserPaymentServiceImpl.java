package com.hp.service.impl;

import com.hp.domain.User;
import com.hp.domain.UserBilling;
import com.hp.domain.UserPayment;
import com.hp.repository.UserBillingRepo;
import com.hp.repository.UserPaymentRepository;
import com.hp.service.UserPaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class UserPaymentServiceImpl implements UserPaymentService {

    @Autowired
    private UserPaymentRepository userPaymentRepository;

    @Autowired
    private UserBillingRepo userBillingRepo;

    public Optional<UserPayment> findById(Long id) {
        return userPaymentRepository.findById(id);
    }

    @Override
    @Transactional
    public void deleteUserPaymentById(Long creditCardId) {
        userPaymentRepository.deleteUserPaymentById(creditCardId);
    }


    @Override
    @Transactional
    public void updateUserBilling(UserBilling userBilling, UserPayment userPayment, User user) {
        userPayment.setUser(user);
        userPayment.setUserBilling(userBilling);
        userPayment.setDefaultPayment(true);
        userBilling.setUserPayment(userPayment);

        userBillingRepo.deleteUserBillingById(userPayment.getId());
        userPaymentRepository.save(userPayment);
    }

    @Override
    public List<UserPayment> getUserPaymentListById(Long userId) {
        return userPaymentRepository.getUserPaymentListById(userId);
    }

}

package com.hp.service.impl;

import com.hp.domain.User;
import com.hp.domain.UserShipping;
import com.hp.repository.UserShippingRepository;
import com.hp.service.UserShippingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class UserShippingServiceImpl implements UserShippingService {

    @Autowired
    private UserShippingRepository userShippingRepository;

    @Override
    public List<UserShipping> getUserShippingListById(Long userId) {
        return userShippingRepository.getUserShippingListById(userId);
    }

    @Override
    public Optional<UserShipping> findById(Long shippingAddressId) {
        return userShippingRepository.findById(shippingAddressId);
    }

    @Override
    @Transactional
    public void removeById(Long userShippingId) {
        userShippingRepository.deleteById2(userShippingId);
    }


}

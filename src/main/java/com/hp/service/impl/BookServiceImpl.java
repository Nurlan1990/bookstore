package com.hp.service.impl;

import com.hp.domain.Book;
import com.hp.domain.DataTableRequest;
import com.hp.domain.DataTableResult;
import com.hp.repository.BookRepository;
import com.hp.repository.customrepo.BookCustomRepository;
import com.hp.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private BookCustomRepository bookCustomRepository;


    @Override
    public Book save(Book book) {
        return bookRepository.save(book);
    }

    @Override
    public List<Book> findAll() {
        return (List<Book>) bookRepository.findAll();
    }

    @Override
    public DataTableResult getCompanyJobDataTableResult(DataTableRequest dataTableRequest, int start, int length) {

        DataTableResult dataTableResult = new DataTableResult();

        dataTableResult.setDraw(dataTableRequest.getDraw());


        Map<Integer,String> columnMap = new HashMap<>();
        columnMap.put(0,"title");
        columnMap.put(1,"author");
        columnMap.put(2,"category");
        columnMap.put(3,"list_price");
        columnMap.put(4,"our_price");
        columnMap.put(5,"active");

        String sortColumn = columnMap.get(dataTableRequest.getSortColumn());
        String sortDirection = dataTableRequest.getSortDirection();
        String searchValue = dataTableRequest.getFilter();
        String startt=String.valueOf(start);
        String lengthh=String.valueOf(length);
        List<Book> bookList = bookCustomRepository.getFilteredSortedBookList(startt,
                lengthh, searchValue,
                sortColumn, sortDirection);

                String link = "<a  href=\"bookInfo?id=%s\">View</a>&nbsp;<a href=\"updateBook?id=%s\">Edit</a>&nbsp;" +
                "<a class=\"delete\" href=\"deleteBook?bookId=%s\">Delete</a>";

        Object[][] data = new Object[bookList.size()][7];
        for (int i = 0; i < data.length; i++) {
            data[i][0] = bookList.get(i).getTitle();
            data[i][1] = bookList.get(i).getAuthor();
            data[i][2] = bookList.get(i).getCategory();
            data[i][3] = bookList.get(i).getListPrice();
            data[i][4] = bookList.get(i).getOurPrice();
            data[i][5] = bookList.get(i).isActive();
            data[i][6] = String.format(link, bookList.get(i).getId(), bookList.get(i).getId(), bookList.get(i).getId());
        }
        dataTableResult.setData(data);

        long totalCount = bookRepository.count();
        dataTableResult.setRecordsTotal(totalCount);

        String searchValue2 = "%"+dataTableRequest.getFilter()+"%";
        long filteredCount = bookRepository.getFilteredBookCount(searchValue2);
        dataTableResult.setRecordsFiltered(filteredCount);

        return dataTableResult;
    }

    @Override
    public Optional<Book> findById(Long id) {
        return bookRepository.findById(id);
    }


}















package com.hp.service.impl;

import com.hp.domain.UserRole;
import com.hp.repository.UserRoleRepository;
import com.hp.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class UserRoleServiceImpl implements UserRoleService {

    @Autowired
    private UserRoleRepository userRoleRepository;


    @Override
    public Set<UserRole> findByUserId(Long id) {
        return userRoleRepository.findByUserId(id);
    }
}

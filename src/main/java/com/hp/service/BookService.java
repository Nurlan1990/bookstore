package com.hp.service;

import com.hp.domain.Book;
import com.hp.domain.DataTableRequest;
import com.hp.domain.DataTableResult;

import java.util.List;
import java.util.Optional;

public interface BookService {
    Book save(Book book);

    List<Book> findAll();


    DataTableResult getCompanyJobDataTableResult(DataTableRequest dataTableRequest, int start, int length);

    Optional<Book> findById(Long id);
}

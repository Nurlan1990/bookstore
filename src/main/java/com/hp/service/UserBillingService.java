package com.hp.service;

public interface UserBillingService {

    void deleteUserBillingById(Long creditCardId);
}

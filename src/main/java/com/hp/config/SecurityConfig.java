package com.hp.config;

import com.hp.security.BookstoreAuthSuccessHandler;
import com.hp.security.UserSecuritySevice;
import com.hp.util.SecurityUtiliy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    @Autowired
    private UserSecuritySevice userSecuritySevice;

    @Autowired
    private BookstoreAuthSuccessHandler authSuccessHandler;


    public BCryptPasswordEncoder passwordEncoder(){
        return SecurityUtiliy.passwordEncoder();
    }


    private static final String[] PUBLIC_MATCHERS = {
           "/static/css/**", "/static/image/**", "/static/js/**", "/", "/myAccount", "/newUser",
            "/forgetPassword", "/login", "/bookSheif","/bookDetail"
    };

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers(PUBLIC_MATCHERS)
                .permitAll()
                .antMatchers("/admin/**").hasRole("ADMIN")
                .antMatchers("/myProfile").hasAnyRole("USER","ADMIN")
                .anyRequest()
                .authenticated();

        http
                .csrf().disable().cors().disable()
                .formLogin()
                .loginPage("/login")
                .failureUrl("/login?error")
               // .defaultSuccessUrl("/myProfile")
                .successHandler(authSuccessHandler)
                .permitAll()
                .and()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/?logout")
                .deleteCookies("remember-me")
                .permitAll()
                .and()
                .rememberMe();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userSecuritySevice).passwordEncoder(passwordEncoder());
    }
}
















package com.hp.repository.customrepo;

import com.hp.domain.Book;

import java.util.List;
import java.util.Map;

public interface BookCustomRepository {

     List<Book> getFilteredSortedBookList(String startt, String lengthh, String searchValue, String sortColumn,
            String sortDirection
    );
}

package com.hp.repository.customrepo;

public class SqlQuery {

    public static final String GET_BOOK_LIST= " select * from book b " +
            "  where concat(b.author,ifnull(b.category,'') , ifnull(b.description,''), " +
            "  ifnull(b.format,''), ifnull(b.isbn,''), ifnull(b.language,''), ifnull(b.publisher,'')) " +
            " like :searchValue " +
            " order by {SORT_COLUMN} {SORT_DIRECTION} " +
            " limit {START}, {LENGTH}";
}

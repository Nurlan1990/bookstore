package com.hp.repository.customrepo.impl;

import com.hp.domain.Book;
import com.hp.repository.customrepo.BookCustomRepository;
import com.hp.repository.customrepo.SqlQuery;
import com.hp.repository.customrepo.rowmapper.BookMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class BookCustomRepositoryImpl implements BookCustomRepository {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    private BookMapper bookMapper;

    public List<Book> getFilteredSortedBookList(String startt, String lengthh, String searchValue, String sortColumn,
            String sortDirection
    ) {

        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("searchValue","%" + searchValue + "%");

        String sql = SqlQuery.GET_BOOK_LIST.replace("{SORT_COLUMN}",sortColumn)
                .replace("{SORT_DIRECTION}", sortDirection)
                .replace("{START}", startt)
                .replace("{LENGTH}", lengthh);

        return jdbcTemplate.query(sql, params, bookMapper);
    }
}

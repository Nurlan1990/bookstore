package com.hp.repository.customrepo.rowmapper;

import com.hp.domain.Book;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class BookMapper implements RowMapper<Book> {
    @Override
    public Book mapRow(ResultSet rs, int rowNum) throws SQLException {

    /*   " select * from book b " +
            "  where concat(b.author,ifnull(b.category,'') , ifnull(b.description,''), " +
            "  ifnull(b.format,''), ifnull(b.isbn,''), ifnull(b.language,''), ifnull(b.publisher,'')) " +
            " like :searchValue " +
            " order by {SORT_COLUMN} {SORT_DIRECTION} " +
            " limit {START}, {LENGTH}";*/

        Book book = new Book();

        if(rs.getLong("id") > 0) {
            book.setId(rs.getLong("id"));
            book.setActive(rs.getBoolean("active"));

            if(rs.getString("author") != null) {
                book.setAuthor(rs.getString("author"));
            }

            if(rs.getString("category") != null) {
                book.setCategory(rs.getString("category"));
            }

            if(rs.getString("title") != null) {
                book.setTitle(rs.getString("title"));
            }

            book.setListPrice(rs.getDouble("list_price"));
            book.setOurPrice(rs.getDouble("our_price"));
        }


        return book;
    }
}


package com.hp.repository;

import com.hp.domain.UserRole;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface UserRoleRepository extends CrudRepository<UserRole, Integer> {


    Set<UserRole> findByUserId(Long id);
}

package com.hp.repository;

import com.hp.domain.UserPayment;
import com.hp.domain.UserShipping;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface UserShippingRepository extends CrudRepository<UserShipping, Long> {

    @Query(value = "select * from user_shipping b where b.user_id = :userId", nativeQuery = true)
    List<UserShipping> getUserShippingListById(@Param("userId") Long userId);

    @Modifying
    @Transactional
    @Query(value = "Delete from user_shipping b where b.id = :userShippingId", nativeQuery = true)
    void deleteById2(@Param("userShippingId") Long userShippingId);


}

package com.hp.repository;

import com.hp.domain.Book;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;



@Repository
public interface BookRepository extends CrudRepository<Book, Long> {


    @Query(value = "select count(b.id) " +
            "from book b " +
            "where concat(b.author, b.category, b.description, " +
            " b.format, b.isbn, b.language, b.publication_date, b.publisher, b.title) " +
            " like :searchValue2", nativeQuery = true)
    long getFilteredBookCount(@Param("searchValue2") String searchValue2);



}

package com.hp.repository;

import com.hp.domain.UserBilling;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface UserBillingRepo extends CrudRepository<UserBilling, Long> {


    @Modifying
    @Transactional
    @Query(value = "Delete from user_billing b where b.user_payment_id = :creditCardId", nativeQuery = true)
    void deleteUserBillingById(@Param("creditCardId") Long creditCardId);


}

package com.hp.repository;

import com.hp.domain.PasswordResetToken;
import com.hp.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {


   // PasswordResetToken getPasswordResetToken(final String token);

    User findByUsername(String username);

    User findByEmail(String email);

    User findUserByUsername(String username);




}

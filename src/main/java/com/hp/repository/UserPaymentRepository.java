package com.hp.repository;

import com.hp.domain.UserPayment;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserPaymentRepository extends CrudRepository<UserPayment, Long> {

    Optional<UserPayment> findById(Long id);



    @Modifying
    @Transactional
    @Query(value = "Delete from user_payment b where b.id = :creditCardId", nativeQuery = true)
    void deleteUserPaymentById(@Param("creditCardId") Long creditCardId);

    @Query(value = "select * from user_payment b where b.user_id = :userId", nativeQuery = true)
    List<UserPayment> getUserPaymentListById(@Param("userId") Long userId);
}

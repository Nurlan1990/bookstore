package com.hp.domain;


import javax.persistence.*;
import java.io.Serializable;

@Entity
public class BookToCartItem implements Serializable {

    private static final long serialVersionUID = -7626426113501183440L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="book_id")
    private Book book;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="cart_item_id")
    private CartItem cartItem;

    public BookToCartItem() {
    }

    public BookToCartItem(Long id, Book book, CartItem cartItem) {
        this.id = id;
        this.book = book;
        this.cartItem = cartItem;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public CartItem getCartItem() {
        return cartItem;
    }

    public void setCartItem(CartItem cartItem) {
        this.cartItem = cartItem;
    }


}


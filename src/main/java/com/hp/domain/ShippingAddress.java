package com.hp.domain;


import javax.persistence.*;
import java.io.Serializable;

@Entity
public class ShippingAddress implements Serializable {

    private static final long serialVersionUID = -4399312294646859392L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String ShippingAddressName;
    private String ShippingAddressStreet1;
    private String ShippingAddressStreet2;
    private String ShippingAddressCity;
    private String ShippingAddressState;
    private String ShippingAddressCountry;
    private String ShippingAddressZipcode;


    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;


    @OneToOne(cascade=CascadeType.ALL)
    private Order order;

    public ShippingAddress() {
    }

    public ShippingAddress(Long id, String shippingAddressName, String shippingAddressStreet1, String shippingAddressStreet2, String shippingAddressCity, String shippingAddressState, String shippingAddressCountry, String shippingAddressZipcode, User user, Order order) {
        this.id = id;
        ShippingAddressName = shippingAddressName;
        ShippingAddressStreet1 = shippingAddressStreet1;
        ShippingAddressStreet2 = shippingAddressStreet2;
        ShippingAddressCity = shippingAddressCity;
        ShippingAddressState = shippingAddressState;
        ShippingAddressCountry = shippingAddressCountry;
        ShippingAddressZipcode = shippingAddressZipcode;
        this.user = user;
        this.order = order;
    }

    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }


    public String getShippingAddressName() {
        return ShippingAddressName;
    }


    public void setShippingAddressName(String shippingAddressName) {
        ShippingAddressName = shippingAddressName;
    }


    public String getShippingAddressStreet1() {
        return ShippingAddressStreet1;
    }


    public void setShippingAddressStreet1(String shippingAddressStreet1) {
        ShippingAddressStreet1 = shippingAddressStreet1;
    }


    public String getShippingAddressStreet2() {
        return ShippingAddressStreet2;
    }


    public void setShippingAddressStreet2(String shippingAddressStreet2) {
        ShippingAddressStreet2 = shippingAddressStreet2;
    }


    public String getShippingAddressCity() {
        return ShippingAddressCity;
    }


    public void setShippingAddressCity(String shippingAddressCity) {
        ShippingAddressCity = shippingAddressCity;
    }


    public String getShippingAddressState() {
        return ShippingAddressState;
    }


    public void setShippingAddressState(String shippingAddressState) {
        ShippingAddressState = shippingAddressState;
    }


    public String getShippingAddressCountry() {
        return ShippingAddressCountry;
    }


    public void setShippingAddressCountry(String shippingAddressCountry) {
        ShippingAddressCountry = shippingAddressCountry;
    }


    public String getShippingAddressZipcode() {
        return ShippingAddressZipcode;
    }


    public void setShippingAddressZipcode(String shippingAddressZipcode) {
        ShippingAddressZipcode = shippingAddressZipcode;
    }


    public User getUser() {
        return user;
    }


    public void setUser(User user) {
        this.user = user;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
}


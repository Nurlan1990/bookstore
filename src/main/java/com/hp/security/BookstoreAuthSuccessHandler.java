package com.hp.security;

import com.hp.domain.User;
import com.hp.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDateTime;

@Component
public class BookstoreAuthSuccessHandler implements AuthenticationSuccessHandler {
    @Autowired
    private UserRoleService userRoleService;


    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response,
                                        Authentication authentication) throws IOException, ServletException {

        User user = (User) authentication.getPrincipal();
        System.out.println("logged in user = " + user);
        HttpSession session = request.getSession();
        session.setAttribute("user", user);
        session.setAttribute("loginTime", LocalDateTime.now());

        boolean hasAdminRole = userRoleService.findByUserId(user.getId()).
                stream().
                anyMatch(userRole -> userRole.getRole().getRoleId()==2);

        String page = "/myProfile";

        if(hasAdminRole){
            page="/admin/";
        }

        response.sendRedirect(request.getContextPath() + page);
    }
}

package com.hp.security;

import com.hp.domain.User;
import com.hp.service.UserRoleService;
import com.hp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserSecuritySevice implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRoleService userRoleService;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> optionalUser = Optional.ofNullable(userService.findByUsername(username));

        if(optionalUser.isPresent()) {
            User user = optionalUser.get();
            user.setUserRoles(userRoleService.findByUserId(user.getId()));

            return user;
        } else {
            throw new UsernameNotFoundException("User " + username + " not found!");
        }



    }
}
